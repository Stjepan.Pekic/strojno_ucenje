import numpy as np
import matplotlib.pyplot as plt


#zad1
'''x = np.array([1,2,3,3,1])
y = np.array([1,2,2,1,1])
plt.plot(x,y, 'b',c='r', linewidth=5, marker='.', markersize=10)
plt.axis([0.0, 4.0, 0.0, 4.0])
plt.xlabel('x os')
plt.ylabel('y os')
plt.title('Primjer')
plt.show()
'''


#zad2
'''data = np.loadtxt('data.csv',  skiprows=1, delimiter=',')
print(f'Number of people: {data[:,0].size}')
height = np.array(data[:,1])
weight = np.array(data[:,2])
plt.figure(1)
plt.scatter(height, weight, s=1)
plt.xlabel('Height(cm)')
plt.ylabel('Weight(kg)')
plt.title('Height and weight relationship')
plt.show()
height50 = np.array(data[::50,1])
weight50 = np.array(data[::50,2])
plt.figure(1)
plt.scatter(height50, weight50, s=1)
plt.xlabel('Height(cm)')
plt.ylabel('Weight(kg)')
plt.title('Height and weight relationship of every 50th person')
plt.show()
print(f'Min height: {np.min(height)} cm')
print(f'Max height: {np.max(height)} cm')
print(f'Average height: {np.mean(height)} cm')
    
indMale = (data[:,0]==1)
indFemale = (data[:,0]==0)
heightMale = (data[:,1][indMale])
heightFemale = (data[:,1][indFemale])

print(f'Min male height: {np.min(heightMale)} cm')
print(f'Max male height: {np.max(heightMale)} cm')
print(f'Average male height: {np.mean(heightMale)} cm')

print(f'Min female height: {np.min(heightFemale)} cm')
print(f'Max female height: {np.max(heightFemale)} cm')
print(f'Average female height: {np.mean(heightFemale)} cm')
'''



#zad3
'''img = plt.imread('road.jpg')
img = img[:,:,0].copy()
plt.imshow(img, cmap='gray')
plt.show()
imgBright = img+150
imgBright[img > 105] = 255
plt.imshow(imgBright, cmap='gray')
plt.show()

columns = img[1,:].size
imgSecondQuarter = img[:,int(columns/4):int(columns/2)]
plt.imshow(imgSecondQuarter, cmap='gray')
plt.show()

imgRot90 = np.rot90(img, axes=(1,0))
plt.imshow(imgRot90, cmap='gray')
plt.show()

imgFlip = np.flip(img, axis=1)
plt.imshow(imgFlip, cmap='gray')
plt.show()
'''



#zad4
'''black1 = np.zeros((50,50))
black2 = black1.copy()
white1 = np.ones((50,50))
white2 = white1.copy()
row1 = np.hstack((black1,white1))
row2 = np.hstack((white2, black2))
full = np.vstack((row1,row2))
plt.figure()
plt.imshow(full, cmap='gray')
plt.show()
'''
