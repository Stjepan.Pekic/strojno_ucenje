import numpy as np
import matplotlib.pyplot as plt

### A
data=np.loadtxt('Ispit/diabetes.csv',delimiter=',',skiprows=1)

print(data.shape[0])
###

### B
data[~np.isnan(data[:,[5,7]]).any(axis=1),:] # odbacuje sve redove koji nemaju unesenu vrijednost
new_data=np.unique(data[:,[5,7]],axis=0) #vraca unique redove po BMI i godinama
print(new_data)
print(data.shape[0])
###

### C
plt.figure()
plt.scatter(data[:,5],data[:,7])
plt.xlabel('BMI')
plt.ylabel('Age')
plt.show()
###

### D
print('Max: ',np.max(data[:,5]))
print('Mean: ',np.mean(data[:,5]))
print('Min: ',np.min(data[:,5]))
###

### E
diabetes=data[data[:,8] == 1]
not_diabetes=data[data[:,8] == 0]
print('Has diabetes: ',len(diabetes))
print('Max: ',np.max(diabetes[:,5]))
print('Mean: ',np.mean(diabetes[:,5]))
print('Min: ',np.min(diabetes[:,5]))
print()
print('Dont have diabetes: ',len(not_diabetes))
print('Max: ',np.max(not_diabetes[:,5]))
print('Mean: ',np.mean(not_diabetes[:,5]))
print('Min: ',np.min(not_diabetes[:,5]))
###





import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.inspection import DecisionBoundaryDisplay
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import MinMaxScaler

data=np.loadtxt('Ispit/diabetes.csv',delimiter=',',skiprows=1)

X=data[:,0:-1]
y=data[:,-1]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

### A
lm=LogisticRegression()
lm.fit(X_train,y_train)
###

### B
y_pred=lm.predict(X_test)
###

### C
cm=confusion_matrix(y_true=y_test,y_pred=y_pred)
ConfusionMatrixDisplay(cm).plot()
###

### D documentation = https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html
print(f'točnost: {(cm[0,0]+cm[1,1])/(cm[0,0]+cm[1,1]+cm[0,1]+cm[1,0])*100}%')
print(f'preciznost: {cm[1,1]/(cm[1,1]+cm[0,1])*100}%')
print(f'odziv: {cm[1,1]/(cm[1,1]+cm[1,0])*100}%')
###

plt.show()





import numpy as np
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from keras.models import load_model
from os.path import exists
import random

data=np.loadtxt('Ispit/diabetes.csv',delimiter=',',skiprows=1)

X=data[:,0:-1]
y=data[:,-1]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

if not exists('Ispit/Neural Network'):
    ### A
    model= keras.Sequential()
    model.add(layers.Input(shape=(8,)))
    model.add(layers.Dense(12, activation='relu'))
    model.add(layers.Dense(8, activation='relu'))
    model.add(layers.Dense(1, activation='sigmoid'))

    model.summary()
    ###

    ### B
    model.compile(loss='categorical_crossentropy',
                optimizer='adam',
                metrics=['accuracy'])
    ###

    ### C
    epoch=20
    batch_size=10
    model.fit(X_train,
            y_train,
            batch_size=batch_size,
            epochs=epoch,
            validation_split=0.1)
    ###

    ### D
    model.save('Ispit/Neural Network')
    ###
else:
    model=load_model('Ispit/Neural Network')

### E
score = model.evaluate(X_test, y_test, verbose=0)
print(score)
###

### F
y_pred=model.predict(X_test)
cm=confusion_matrix(y_true=y_test,y_pred=y_pred)
ConfusionMatrixDisplay(cm).plot()
###

plt.show()
